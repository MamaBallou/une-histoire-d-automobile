import os #Pour vérifier que le fichier existe et le supprimer
import csv #Pour lire les fichiers .csv
import datetime

#Fonction de découpage de lignes d'un fichier
#Paramètre : les nom du fichier, <string>
#Retourne : un tableau de chaine de caractères, sans compter l'en-tête
def decoupage(fic) :
    #Vérifie que le fichier existe
    if ((os.path.exists(fic) and os.path.isfile(fic))== False):
        return 1
    #Initialise le type de la variable
    tabFinal = []
    #Ouvre le fichier
    with open(fic, newline='') as csvFile :
        reader = csv.reader(csvFile, delimiter='|')
        #Pour chaque ligne on extrait les deux derniere valeurs,
        # que l'on stocke dans des variables tampons
        #On découpe la variable t_v_vTmp corespondant à type_variante_version
        # que l'on redécoupe en 3, avec pour séparateur ', '
        #On fini par remettre toutes les valeurs dans tabFinal
        for row in reader :
            if len(row) >= 2 :
                vinTmp = row.pop()
                t_v_vTmp = row.pop()
                row = row + t_v_vTmp.split(', ')
                row.append(vinTmp)
                tabFinal.append(row)
        #On retire l'en-tête
        if tabFinal :
            tabFinal.pop(0)
    #On ferme le fichier
    csvFile.close()
    return tabFinal


#Cette fonction permet de modifier l'ordre de l'entête du document.
# Cette dernière prend, en paramètre : un tableau et un caractère
# Elle retourne : un fichier ".csv"
def changementOrdre(tab, char):

    #Nous commençons d'abord par ouvrir le document "autoRes.csv"
    # (si il n'existe pas, le programme se permet de créer ce document
    # pour nous).
    with open ('autoRes.csv', 'w', newline='') as csvFile :

        #Nous créons un tableau avec le bon ordre des champs d'entête.
        # Nous pourons l'utiliser pour l'écrire dans le document par la suite
        fieldnames = ['adresse_titulaire', 'nom', 'prenom', 'immatriculation', 
        'date_immatriculation', 'vin', 'marque', 'denomination_commerciale', 
        'couleur', 'carrosserie', 'categorie', 'cylindree', 'energie', 'places', 
        'poids', 'puissance', 'type', 'variante', 'version',]
        writer = csv.DictWriter(csvFile, fieldnames, delimiter=char)
        writer.writeheader()

        #À partir de là, nous allons faire une boucle en fonction du tableau
        # prit en paramètre en fcontion des différentes lignes pour les
        # insérer dans notre document. Comme nous pouvons le voir, ces
        # valeurs rentrées ne seront pas dans le même ordre qu'auparavent,
        # puisque l'ordre des champs de l'entête est modifié.
        for lines in tab :
            writer.writerow({
                'adresse_titulaire': lines[0],
                'nom': lines[11],
                'prenom': lines[8],
                'immatriculation': lines[9],
                'date_immatriculation': convertDate(lines[5]),
                'vin': lines[18],
                'marque': lines[10],
                'denomination_commerciale': lines[6],
                'couleur': lines[3],
                'carrosserie': lines[1],
                'categorie': lines[2],
                'cylindree': lines[4],
                'energie': lines[7],
                'places': lines[12],
                'poids': lines[13],
                'puissance': lines[14],
                'type': lines[15],
                'variante': lines[16],
                'version': lines[17]
            })
        return 'autoRes.csv'


#Cette fonction permet de changer le format d'une date passéef en
# paramètre pour renvoyer la date avec le nouveau format
def convertDate (date) :
    current_date = datetime.datetime.strptime(date, '%Y-%m-%d')
    return current_date.strftime('%d/%m/%Y')