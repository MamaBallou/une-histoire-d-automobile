import unittest
import csv
import os
from fonctions import decoupage
from fonctions import changementOrdre

class TestTraductionFunction(unittest.TestCase):

    #Est exécuter au début de chaque test
    #Créer un fichier test.csv et supprime autoRes.csv s'il existe
    def setUp(self) :
         file = open('test.csv', 'w')
         file.close()
         if(os.path.exists('autoRes.csv')):
            os.remove('autoRes.csv')
    
    #Est exécuter à la fin de chaque test    
    def tearDown(self) :
        os.remove('test.csv')

    #Test de la fonction découpage
    def test_decoupage(self):
        #On remplie test.csv avec des valeurs testes
        with open('test.csv', 'w', newline='') as tstFile :
            fieldnames = ['address', 'carrosserie', 'categorie', 'couleur', 
            'cylindree', 'date_immat', 'denomination', 'energy', 'firstname', 
            'immat', 'marque', 'name', 'places', 'poids', 'puissance', 
            'type_variante_version', 'vin']
            writer = csv.DictWriter(tstFile, fieldnames, delimiter='|')
            writer.writeheader()
            writer.writerow({
                'address': '3822 Omar Square Suite 257 Port Emily, OK 43251',
                'carrosserie': '45-1743376',
                'categorie': '34-7904216',
                'couleur': 'LightGoldenRodYellow',
                'cylindree': '3462',
                'date_immat': '2012-05-03',
                'denomination': 'Enhanced well-modulated moderator',
                'energy': '37578077',
                'firstname': 'Jerome',
                'immat': 'OVC-568',
                'marque': 'Williams Inc',
                'name': 'Smith',
                'places': '32',
                'poids': '3827',
                'puissance': '110',
                'type_variante_version': 'Inc, 92-3625175, 79266482',
                'vin': '9780082351764',
            })

            #Créer le tableau : résulat, attendu
            resultat = [['3822 Omar Square Suite 257 Port Emily, OK 43251', 
            '45-1743376', '34-7904216', 'LightGoldenRodYellow', '3462', 
            '2012-05-03', 'Enhanced well-modulated moderator', '37578077', 
            'Jerome', 'OVC-568', 'Williams Inc', 'Smith', '32', '3827', '110', 
            'Inc', '92-3625175', '79266482', '9780082351764',]]

        #Initialise 2 compteur pour itérer le tableau
        compt1 = 0 
        compt2 = 0
        #Parcours le tableau retourner par la fonction découpage
        for ligne in decoupage('test.csv'):
            for val in ligne:
                #Compare les valeurs
                self.assertEqual(val, resultat[compt1][compt2])
                compt2+=1
            compt1+=1
        #Ferme de fichier test.csv
        tstFile.close()
    
    def test_changementOrdre(self):
        #Ici, nous créons une liste ayant en valeurs des données
        # à trier (donc une liste à trier)
        aTester = [['3822 Omar Square Suite 257 Port Emily, OK 43251',
         '45-1743376', '34-7904216', 'LightGoldenRodYellow', '3462',
          '2012-05-03', 'Enhanced well-modulated moderator', '37578077'
          , 'Jerome', 'OVC-568', 'Williams Inc', 'Smith', '32', '3827', '110',
           'Inc', '92-3625175', '79266482', '9780082351764',]]

        #Nous créons ensuite un fichier appelé "test.csv" qui aura en valeur 
        #les mêmes que la liste "aTester" mais qui sont triées.
        with open('test.csv', 'w', newline='') as tstFile :

            #Nous entrons l'entête de notre document qui montrera quelle
            #  valeur correspond à quelle donnée
            fieldnames = ['adresse_titulaire', 'nom', 'prenom', 
            'immatriculation', 'date_immatriculation', 'vin', 'marque', 
            'denomination_commerciale', 'couleur', 'carrosserie', 'categorie', 
            'cylindree', 'energie', 'places', 'poids', 'puissance', 'type', 
            'variante', 'version',]

            writer = csv.DictWriter(tstFile, fieldnames, delimiter=';')
            writer.writeheader()

            #Nous pouvons voir que les valeurs triées sont entrées de paire avec
            #  les noms des données
            writer.writerow({
                'adresse_titulaire': 
                    '3822 Omar Square Suite 257 Port Emily, OK 43251',
                'nom': 'Smith',
                'prenom': 'Jerome',
                'immatriculation': 'OVC-568',
                'date_immatriculation': '2012-05-03',
                'vin': '9780082351764',
                'marque': 'Williams Inc',
                'denomination_commerciale': 
                    'Enhanced well-modulated moderator',
                'couleur': 'LightGoldenRodYellow',
                'carrosserie': '45-1743376',
                'categorie': '34-7904216',
                'cylindree': '3462',
                'energie': '37578077',
                'places': '32',
                'poids': '3827',
                'puissance': '110',
                'type': 'Inc',
                'variante': '92-3625175',
                'version': '79266482'
            })
        tstFile.close()

        compteur1 = 0
        compteur2 = 0
        
        #Ici, nous allons tester la fonction changementOrdre en utilisant la
        #  fonction decoupage, pour vérifier si nos valeurs ont bien étés 
        # changées de place et ont bien un nom de donnée différent
        for ligne in decoupage('test.csv') :
            for val in ligne[1:] :
                self.assertEqual(val, decoupage(changementOrdre(aTester,';'))[compteur1][compteur2])
                compteur2+=1
            compteur1+=1
        #Nous devons commencer notre vérification à partir de la deuxième ligne
        # de notre document, car la première est l'entête. Ensuite, nous allons
        # vérifier une à une les lignes ET les valeurs :
        # Nous commencons par la première valeurs d'une ligne, puis la suivante
        # et, quand nous avons atteints la fin de la ligne, nous passons à la
        # suivante, jusqu'à avoir finit le document.