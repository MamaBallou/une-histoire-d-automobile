import csv
import argparse
import sys
from fonctions import *

parser = argparse.ArgumentParser(description="Vous devez passer les arguements suivants :")
parser.add_argument("<fic>", help="Le nom du fichier '.csv' à convertir de ProSiv vers FleetManager")
parser.add_argument("<delimiter>", help="Le délimiter (ne pas oublier de le mettre entre côte)")
args = parser.parse_args()

if len(sys.argv)==3 :     
    changementOrdre(decoupage(sys.argv[1]),sys.argv[2])